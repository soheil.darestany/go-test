package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"math/rand"
	"net/http"
	postgresSql "test/pkg/db"
	"test/pkg/models"
)

func main() {
	r := gin.Default()

	r.GET("insert", func(c *gin.Context) {
		user := models.User{
			Name:   RandStringBytes(5),
			Family: RandStringBytes(4),
		}
		postgresSql.DB.Create(&user)
		c.JSON(http.StatusCreated, gin.H{"data": user})
	})
	r.GET("select", func(c *gin.Context) {
		var user models.User
		postgresSql.DB.Last(&user)
		c.JSON(http.StatusCreated, gin.H{"data": user})
	})
	err := r.Run(":8080")
	if err != nil {
		fmt.Println()
	}
}

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

func RandStringBytes(n int) string {
	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
	}
	return string(b)
}
