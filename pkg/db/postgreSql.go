package postgresSql

import (
	"fmt"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/plugin/dbresolver"
	"test/pkg/config"
	"test/pkg/models"
	"time"
)

var DB *gorm.DB

type connection struct {
	Dsn1 string
	Dsn2 string
	Dsn3 string
}

func NewConnection() *connection {
	db := config.Env.Db
	return &connection{
		Dsn1: db.Dsn1,
		Dsn2: db.Dsn2,
		Dsn3: db.Dsn3,
	}
}

func (c *connection) Connect() {
	var err error
	DB, err = gorm.Open(postgres.Open(c.Dsn1), &gorm.Config{})
	if err != nil {
		fmt.Println(err)
		return
	}

	err = DB.Use(dbresolver.Register(dbresolver.Config{
		Sources: []gorm.Dialector{postgres.Open(c.Dsn1), postgres.Open(c.Dsn2), postgres.Open(c.Dsn3)},
		Policy:  dbresolver.RandomPolicy{},
	}, &models.User{}).SetConnMaxIdleTime(time.Minute * 5).SetMaxIdleConns(10).SetMaxOpenConns(100).SetConnMaxLifetime(time.Hour))
	if err != nil {
		fmt.Println(err)
		return
	}
}
