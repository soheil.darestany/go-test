package config

import (
	"test/pkg/config/environment"
)

var Env = newEnvData()

type envData struct {
	Db db
}

type db struct {
	Dsn1 string
	Dsn2 string
	Dsn3 string
}

func newEnvData() *envData {
	env := environment.Load()
	return &envData{
		Db: db{
			Dsn1: env.Get("db.Dsn1").(string),
			Dsn2: env.Get("db.Dsn2").(string),
			Dsn3: env.Get("db.Dsn3").(string),
		},
	}
}
