package models

import "gorm.io/gorm"

type User struct {
	gorm.Model
	Name   string `gorm:"size:50"`
	Family string `gorm:"size:50"`
}
