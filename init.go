package main

import (
	postgresSql "test/pkg/db"
	"test/pkg/models"
)

func init() {
	postgresSql.NewConnection().Connect()
	_ = postgresSql.DB.AutoMigrate(&models.User{})
}
